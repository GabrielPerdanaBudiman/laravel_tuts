<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function registrasi(){
        return view('pages.register');
    }

    public function kirim(Request $request){
        $awal = $request['nama_awal'];
        $akhir = $request['nama_akhir'];

        return view('pages.welcome', compact('awal','akhir'));
    }
}
