@extends('layout.master')
@section('judul')
Buat Account Baru!
@endsection

@section('isi')
<h3>Sign Up Form</h3>
<form action="/sent" method="post">
    @csrf
    <p>First Name:</p>
    <input type="text" name="nama_awal">
    <p>Last Name :</p>
    <input type="text" name="nama_akhir"> 
    <p>Gender :</p>
    <input type="radio" name="gender" id="male"><label for="male">Male</label><br>
    <input type="radio" name="gender" id="female"><label for="female">Female</label><br>
    <input type="radio" name="gender" id="other"><label for="other">Other</label><br>
    <p>Nationality :</p>
    <select name="" id="">
        <option value="">Indonesian</option>
        <option value="">Singaporean</option>
        <option value="">Malaysian</option>
        <option value="">Australian</option>
    </select>
    <p>Language Spoken :</p>
    <input type="checkbox" name="language" id="bahasa"><label for="bahasa">Bahasa Indonesia</label><br>
    <input type="checkbox" name="language" id="english"><label for="english">English</label><br>
    <input type="checkbox" name="language" id="other"><label for="other">Other</label><br>
    <p>Bio :</p>
    <textarea name="bio" id="" cols="30" rows="10"></textarea><br>
    <input type="submit" value="Sign Up" >
</form>
@endsection