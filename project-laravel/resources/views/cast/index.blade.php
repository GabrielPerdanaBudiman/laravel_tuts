@extends('layout.master')
@section('judul')
List Cast
@endsection

@section('isi')

<a href="/cast/create" class="btn btn-dark mb-3">Tambah Data</a>

<table class="table">
    <thead class="thead-dark">
      <tr>
        <th scope="col">No</th>
        <th scope="col">Nama</th>
        <th scope="col">Umur</th>
        <th scope="col">Bio</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($cast as $key => $item)
            <tr>
                <td>{{$key + 1}}</td> 
                <td>{{$item->nama}}</td> 
                <td>{{$item->umur}}</td>
                <td>{{$item->bio}}</td>
                <td>
                    <form action="/cast/{{$item->id}}" method="POST">
                        @csrf
                        @method('delete')
                        <a href="/cast/{{$item->id}}" class="btn btn-dark">Detail</a>
                        <a href="/cast/{{$item->id}}/edit" class="btn btn-dark">Edit</a>
                        <input type="submit" class="btn btn-dark" value="Delete">
                    </form>
                </td>     
            </tr>  
        @empty
            <tr>
                <td>Data ora ada euy!</td>
            </tr>
        @endforelse
    </tbody>
  </table>
@endsection